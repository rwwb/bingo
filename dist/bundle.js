(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * The classes to create a Bingo game
 */
var GameModes;
(function (GameModes) {
    GameModes[GameModes["mixed"] = 0] = "mixed";
    GameModes[GameModes["showA"] = 1] = "showA";
    GameModes[GameModes["showB"] = 2] = "showB";
})(GameModes = exports.GameModes || (exports.GameModes = {}));
;
// the Bingo app
var BingoGame = /** @class */ (function () {
    // The constructor
    function BingoGame(current, past) {
        this.busy = false;
        this.current = current;
        this.past = past;
        this._game_mode = GameModes.mixed;
        this._content = new Array();
        this._used = new Array();
    }
    Object.defineProperty(BingoGame.prototype, "gameMode", {
        get: function () {
            return this._game_mode;
        },
        set: function (gm) {
            this._game_mode = gm;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BingoGame.prototype, "content", {
        get: function () {
            return this._content;
        },
        // (shallow) copy the content
        set: function (cn) {
            this._content = cn.slice();
            this._used = new Array();
        },
        enumerable: true,
        configurable: true
    });
    // do the next draw
    BingoGame.prototype.next = function () {
        if (this.busy) {
            console.log("already running next or reset");
            return -1;
        }
        this.busy = true;
        var index = this._next();
        this.busy = false;
        if (index == -1) {
            this.current("Game over");
        }
        return index;
    };
    // reset the game
    BingoGame.prototype.reset = function () {
        if (this.busy) {
            console.log("already running next or reset");
            return;
        }
        this.busy = true;
        this._reset();
        this.busy = false;
    };
    // create a new bingo card
    BingoGame.prototype.createCard = function (rows, columns) {
        var retval = [];
        // really inefficient, but the simplest way to make sure we don't select doubles
        var content = this._content.slice();
        // foreach row add a new row to the output
        for (var r = 0; r < rows; r++) {
            retval.push([]);
            // pick a label from the content
            for (var c = 0; c < columns; c++) {
                // no more elements left to chose from
                if (content.length == 0) {
                    retval[r].push("");
                    continue;
                }
                var idx = Math.floor(Math.random() * content.length);
                var value = content.splice(idx, 1)[0];
                // select the label
                var label = "";
                switch (this._game_mode) {
                    case GameModes.mixed:
                        label = value[Math.floor(Math.random() * 2)];
                        break;
                    case GameModes.showA:
                        label = value[1];
                        break;
                    case GameModes.showB:
                        label = value[0];
                        break;
                    default:
                        break;
                }
                // add the label to the return value
                retval[r].push(label);
            }
        }
        return (retval);
    };
    //
    // Private functions
    //
    BingoGame.prototype._next = function () {
        // game over out of elements
        if (this._content.length == 0) {
            return -1;
        }
        // choose the next value to get and remove it from the content
        var index = Math.floor(Math.random() * this._content.length);
        var value = this._content.splice(index, 1)[0];
        // first add it to the already used data
        this._used.push(value);
        // Get thedisplay label
        var label = "";
        var other = "";
        switch (this._game_mode) {
            case GameModes.mixed:
                var i = Math.floor(Math.random() * 2);
                label = value[i];
                other = i == 1 ? value[0] : value[1];
                break;
            case GameModes.showA:
                label = value[0];
                other = value[1];
                break;
            case GameModes.showB:
                other = value[0];
                label = value[1];
                break;
            default:
                break;
        }
        // Show the display label and add it to the list
        this.current(label);
        // append the value to the list
        this.past(label, other);
        // return the index of the current pick
        return index;
    };
    BingoGame.prototype._reset = function () {
        for (var i = 0; i < this._used.length; i++) {
            this._content.push(this._used[i]);
        }
        this._used = new Array();
    };
    return BingoGame;
}());
exports.BingoGame = BingoGame;
},{}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var bingo_1 = require("./bingo");
var renderers_1 = require("./renderers");
// Main game initialization
//
function initialize() {
    // Prepare the renderers
    var cardrenderer = new renderers_1.CardRenderer(document.getElementById("cardholder"));
    var picked = new renderers_1.LabelRenderer(document.getElementById("current_pick"));
    var answers = new renderers_1.AnswerRenderer(document.getElementById("answer_table"));
    // Setup the Bingo game
    var bingo = new bingo_1.BingoGame(function (label) {
        picked.show(label);
    }, function (label, other) {
        answers.add(label, other);
    });
    document.getElementById("reset").addEventListener("click", function () {
        bingo.reset();
        picked.reset();
        answers.reset();
    }, false);
    document.getElementById("settings").addEventListener("click", function () {
        var el = document.getElementById("options");
        el.style.visibility = el.style.visibility == "hidden" ? "visible" : "hidden";
    }, false);
    document.getElementById("list").addEventListener("click", function () {
        answers.showAnswers = false;
        answers.show();
        var el = document.getElementById("answers");
        el.style.visibility = el.style.visibility == "hidden" ? "visible" : "hidden";
    }, false);
    document.getElementById("show_answers").addEventListener("click", function () {
        answers.showAnswers = answers.showAnswers ? false : true;
        answers.show();
    }, false);
    document.getElementById("makecard").addEventListener("click", function () {
        var el = document.getElementById("cards");
        el.style.visibility = el.style.visibility == "hidden" ? "visible" : "hidden";
    }, false);
    //
    // Timer functions
    //
    // Add the bingo controls
    document.getElementById("next").addEventListener("click", function () {
        bingo.next();
        document.getElementById("options").style.visibility = "hidden";
    }, false);
    var timer = undefined;
    var updatetime = 500;
    document.getElementById("run").addEventListener("click", function () {
        // get the interval
        var el = document.getElementById("interval");
        var timespan = parseFloat(el.value) * 1000;
        // call bingo next when the timer reaches 0
        document.getElementById("countdown").textContent = Math.round(timespan / 1000) + " s";
        var curtime = timespan;
        timer = setInterval(function () {
            curtime -= updatetime;
            if (curtime == 0) {
                bingo.next();
                curtime = timespan;
            }
            document.getElementById("countdown").innerText = Math.round(curtime / 1000) + " s";
        }, updatetime);
        // set the options to hidden
        document.getElementById("options").style.visibility = "hidden";
        bingo.next();
    }, false);
    document.getElementById("stop").addEventListener("click", function () {
        console.log("Stop called");
        if (timer)
            clearInterval(timer);
        timer = undefined;
    }, false);
    //
    // Game altering functions
    //
    // select the game mode
    document.getElementById("gametype_1").addEventListener("change", function (evt) {
        if (evt.target.checked)
            bingo.gameMode = bingo_1.GameModes.mixed;
    }, false);
    document.getElementById("gametype_2").addEventListener("change", function (evt) {
        if (evt.target.checked)
            bingo.gameMode = bingo_1.GameModes.showA;
    }, false);
    document.getElementById("gametype_3").addEventListener("change", function (evt) {
        if (evt.target.checked)
            bingo.gameMode = bingo_1.GameModes.showB;
    }, false);
    // add the content loader
    document.getElementById("files").addEventListener("change", function (evt) {
        // only load the first file
        var curfile = evt.target.files[0];
        var reader = new FileReader();
        reader.onload = function () {
            // get the raw text 
            var raw = reader.result;
            // split the string on newlines and tabs
            var res = raw.split("\n").map(function (line) {
                var el = line.split("\t");
                if (el.length != 2)
                    return;
                return [el[0], el[1]];
            });
            // add the content to bingo
            bingo.content = res.filter(function (el) { return el != undefined; });
            answers.reset();
            picked.reset();
        };
        reader.readAsText(curfile);
    }, false);
    //
    // Card generation
    //
    document.getElementById("generate").addEventListener("click", function () {
        // 
        var width = parseInt(document.getElementById("card_width").value);
        var height = parseInt(document.getElementById("card_height").value);
        var number = parseInt(document.getElementById("card_number").value);
        // generate number cards
        var cards = new Array();
        for (var idx = 0; idx < number; idx++) {
            var card = bingo.createCard(height, width);
            cards.push(card);
        }
        // show the cards onscreen
        cardrenderer.show(cards);
    }, false);
    document.getElementById("card_download").addEventListener("click", function () {
        // adapted from https://jsfiddle.net/78xa14vz/3/
        var css = "<style>" +
            '@page WordSection1{size: 595.35pt 841.95pt;mso-page-orientation: portrait;}' +
            'div.WordSection1 {page: WordSection1;}' +
            'table{border-collapse:collapse;}td{border:1px gray solid;width:5em;padding:2px;}' +
            "</style>";
        var html = "<div class='WordSection1'>" + cardrenderer.tag.innerHTML + "</div>";
        var blob = new Blob(['\ufeff', css + html], {
            type: 'application/msword'
        });
        // download in IE
        if (navigator.msSaveOrOpenBlob) {
            navigator.msSaveOrOpenBlob(blob, "bingo_cards.doc");
            // download in other browsers
        }
        else {
            var url = URL.createObjectURL(blob);
            var link = document.createElement('A');
            link.href = url;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }, false);
}
// initialize the game
initialize();
},{"./bingo":1,"./renderers":3}],3:[function(require,module,exports){
"use strict";
/**
 * Renderers to show dynaomic content from the Bingo game
 *
 */
Object.defineProperty(exports, "__esModule", { value: true });
// A class to render labels on the main screen.
var LabelRenderer = /** @class */ (function () {
    function LabelRenderer(tag) {
        this.tag = tag;
    }
    LabelRenderer.prototype.show = function (label) {
        this.tag.innerHTML = label;
    };
    LabelRenderer.prototype.reset = function () {
        this.tag.innerHTML = "Click next";
    };
    return LabelRenderer;
}());
exports.LabelRenderer = LabelRenderer;
// a class to show the answers
var AnswerRenderer = /** @class */ (function () {
    function AnswerRenderer(tag) {
        this.tag = tag;
        this.answers = Array();
        this._showAnswers = false;
    }
    Object.defineProperty(AnswerRenderer.prototype, "showAnswers", {
        get: function () {
            return this._showAnswers;
        },
        set: function (val) {
            this._showAnswers = val;
        },
        enumerable: true,
        configurable: true
    });
    AnswerRenderer.prototype.add = function (label, answer) {
        this.answers.push([label, answer]);
    };
    AnswerRenderer.prototype.show = function () {
        if (this.answers.length == 0) {
            this.tag.innerHTML = "No previous picks";
            return;
        }
        // with answers
        if (this.showAnswers) {
            var text = "<table><tr><th>#</th><th>Shown</th><th>Synonym</th></tr>";
            for (var i = 0; i < this.answers.length; i++) {
                var x = this.answers[i];
                text += "<tr><td>" + (i + 1) + "</td><td>" + x[0] + "</td><td>" + x[1] + "</td></tr>";
            }
            text += "</table>";
            this.tag.innerHTML = text;
            // without answers
        }
        else {
            var text = "<table><tr><th>#</th><th>Shown</th></tr>";
            for (var i = 0; i < this.answers.length; i++) {
                var x = this.answers[i];
                text += "<tr><td>" + (i + 1) + "</td><td>" + x[0] + "</td></tr>";
            }
            text += "</table>";
            this.tag.innerHTML = text;
        }
    };
    AnswerRenderer.prototype.reset = function () {
        this.answers = Array();
        this.tag.innerHTML = "";
    };
    return AnswerRenderer;
}());
exports.AnswerRenderer = AnswerRenderer;
// Render a bingo card
var CardRenderer = /** @class */ (function () {
    function CardRenderer(tag) {
        this.tag = tag;
    }
    CardRenderer.prototype.table = function (card) {
        var text = "<table>";
        for (var i = 0; i < card.length; i++) {
            text += "<tr>";
            for (var k = 0; k < card[i].length; k++) {
                text += "<td>" + card[i][k] + "</td>";
            }
            text += "</tr>";
        }
        text += "</table>";
        return text;
    };
    CardRenderer.prototype.show = function (cards) {
        this.tag.innerHTML = "";
        for (var idx = 0; idx < cards.length; idx++) {
            var card = cards[idx];
            this.tag.innerHTML += this.table(card);
            this.tag.innerHTML += "<br />";
        }
    };
    return CardRenderer;
}());
exports.CardRenderer = CardRenderer;
},{}]},{},[2])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvYmluZ28udHMiLCJzcmMvbWFpbi50cyIsInNyYy9yZW5kZXJlcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OztBQ0NBOztHQUVHO0FBQ0gsSUFBWSxTQUErQjtBQUEzQyxXQUFZLFNBQVM7SUFBRSwyQ0FBSyxDQUFBO0lBQUUsMkNBQUssQ0FBQTtJQUFFLDJDQUFLLENBQUE7QUFBQSxDQUFDLEVBQS9CLFNBQVMsR0FBVCxpQkFBUyxLQUFULGlCQUFTLFFBQXNCO0FBQUMsQ0FBQztBQUU3QyxnQkFBZ0I7QUFDaEI7SUFZSSxrQkFBa0I7SUFDbEIsbUJBQVksT0FBNkIsRUFBRSxJQUE2QztRQUNwRixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBRTtRQUNuQixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBRTtRQUN4QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBRTtRQUVsQixJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUU7UUFDbkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLEtBQUssRUFBb0IsQ0FBRTtRQUMvQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksS0FBSyxFQUFvQixDQUFFO0lBQ2hELENBQUM7SUFFRCxzQkFBSSwrQkFBUTthQUlaO1lBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUU7UUFDNUIsQ0FBQzthQU5ELFVBQWEsRUFBYTtZQUN0QixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBRTtRQUMxQixDQUFDOzs7T0FBQTtJQU9ELHNCQUFJLDhCQUFPO2FBS1g7WUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBRTtRQUMxQixDQUFDO1FBUkQsNkJBQTZCO2FBQzdCLFVBQVksRUFBMkI7WUFDbkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUU7WUFDNUIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLEtBQUssRUFBb0IsQ0FBRTtRQUNoRCxDQUFDOzs7T0FBQTtJQU1ELG1CQUFtQjtJQUNuQix3QkFBSSxHQUFKO1FBQ0ksRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBLENBQUM7WUFDVixPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixDQUFDLENBQUU7WUFDOUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFFO1FBQ2YsQ0FBQztRQUNELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFFO1FBQ2xCLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBRTtRQUMxQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBRTtRQUNuQixFQUFFLENBQUEsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ1osSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBRTtRQUMvQixDQUFDO1FBQ0QsTUFBTSxDQUFDLEtBQUssQ0FBRTtJQUNsQixDQUFDO0lBRUQsaUJBQWlCO0lBQ2pCLHlCQUFLLEdBQUw7UUFDSSxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUEsQ0FBQztZQUNWLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLENBQUMsQ0FBRTtZQUM5QyxNQUFNLENBQUU7UUFDWixDQUFDO1FBQ0QsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUU7UUFDbEIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFFO1FBQ2YsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUU7SUFDdkIsQ0FBQztJQUVELDBCQUEwQjtJQUMxQiw4QkFBVSxHQUFWLFVBQVcsSUFBWSxFQUFFLE9BQWU7UUFDcEMsSUFBSSxNQUFNLEdBQXlCLEVBQUUsQ0FBRTtRQUV2QyxnRkFBZ0Y7UUFDaEYsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBRTtRQUVyQywwQ0FBMEM7UUFDMUMsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLEVBQUMsQ0FBQztZQUN0QixNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFO1lBRWpCLGdDQUFnQztZQUNoQyxHQUFHLENBQUEsQ0FBQyxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBQyxDQUFDO2dCQUV6QixzQ0FBc0M7Z0JBQ3RDLEVBQUUsQ0FBQSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUEsQ0FBQztvQkFDcEIsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRTtvQkFDcEIsUUFBUSxDQUFFO2dCQUNkLENBQUM7Z0JBRUQsSUFBSSxHQUFHLEdBQVcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFFO2dCQUM5RCxJQUFJLEtBQUssR0FBcUIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUU7Z0JBRXpELG1CQUFtQjtnQkFDbkIsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFFO2dCQUNoQixNQUFNLENBQUEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUEsQ0FBQztvQkFDcEIsS0FBSyxTQUFTLENBQUMsS0FBSzt3QkFDaEIsS0FBSyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFFO3dCQUM5QyxLQUFLLENBQUU7b0JBQ1gsS0FBSyxTQUFTLENBQUMsS0FBSzt3QkFDaEIsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBRTt3QkFDbEIsS0FBSyxDQUFFO29CQUNYLEtBQUssU0FBUyxDQUFDLEtBQUs7d0JBQ2hCLEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUU7d0JBQ2xCLEtBQUssQ0FBRTtvQkFDWDt3QkFDSSxLQUFLLENBQUU7Z0JBQ2YsQ0FBQztnQkFDRCxvQ0FBb0M7Z0JBQ3BDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUU7WUFDM0IsQ0FBQztRQUNMLENBQUM7UUFDRCxNQUFNLENBQUEsQ0FBQyxNQUFNLENBQUMsQ0FBRTtJQUNwQixDQUFDO0lBRUQsRUFBRTtJQUNGLG9CQUFvQjtJQUNwQixFQUFFO0lBRU0seUJBQUssR0FBYjtRQUVJLDRCQUE0QjtRQUM1QixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQzFCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUNiLENBQUM7UUFFRCw4REFBOEQ7UUFDOUQsSUFBSSxLQUFLLEdBQVcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBRTtRQUN0RSxJQUFJLEtBQUssR0FBcUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFFO1FBRWpFLHdDQUF3QztRQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBRTtRQUV4Qix1QkFBdUI7UUFDdkIsSUFBSSxLQUFLLEdBQVcsRUFBRSxDQUFFO1FBQ3hCLElBQUksS0FBSyxHQUFXLEVBQUUsQ0FBRTtRQUN4QixNQUFNLENBQUEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUEsQ0FBQztZQUNwQixLQUFLLFNBQVMsQ0FBQyxLQUFLO2dCQUNoQixJQUFJLENBQUMsR0FBVyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBRTtnQkFDL0MsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBRTtnQkFDbEIsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFFO2dCQUN0QyxLQUFLLENBQUU7WUFDWCxLQUFLLFNBQVMsQ0FBQyxLQUFLO2dCQUNoQixLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFFO2dCQUNsQixLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFFO2dCQUNsQixLQUFLLENBQUU7WUFDWCxLQUFLLFNBQVMsQ0FBQyxLQUFLO2dCQUNoQixLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFFO2dCQUNsQixLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFFO2dCQUNsQixLQUFLLENBQUU7WUFDWDtnQkFDSSxLQUFLLENBQUU7UUFDZixDQUFDO1FBRUQsZ0RBQWdEO1FBQ2hELElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUU7UUFFckIsK0JBQStCO1FBQy9CLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFFO1FBRXpCLHVDQUF1QztRQUN2QyxNQUFNLENBQUMsS0FBSyxDQUFFO0lBQ2xCLENBQUM7SUFFTywwQkFBTSxHQUFkO1FBQ0ksR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQyxDQUFDO1lBQ25DLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBRTtRQUN2QyxDQUFDO1FBQ0QsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLEtBQUssRUFBb0IsQ0FBRTtJQUNoRCxDQUFDO0lBQ0wsZ0JBQUM7QUFBRCxDQXZLQSxBQXVLQyxJQUFBO0FBdktZLDhCQUFTOzs7O0FDTnRCLGlDQUFnRDtBQUNoRCx5Q0FBMEU7QUFFMUUsMkJBQTJCO0FBQzNCLEVBQUU7QUFDRjtJQUNJLHdCQUF3QjtJQUN4QixJQUFJLFlBQVksR0FBRyxJQUFJLHdCQUFZLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFFO0lBQzVFLElBQUksTUFBTSxHQUFHLElBQUkseUJBQWEsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUU7SUFDekUsSUFBSSxPQUFPLEdBQUcsSUFBSSwwQkFBYyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBRTtJQUUzRSx1QkFBdUI7SUFDdkIsSUFBSSxLQUFLLEdBQUcsSUFBSSxpQkFBUyxDQUNyQixVQUFDLEtBQWE7UUFDVixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFFO0lBQ3hCLENBQUMsRUFDRCxVQUFDLEtBQWEsRUFBRSxLQUFhO1FBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzlCLENBQUMsQ0FDSixDQUFFO0lBR0gsUUFBUSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUU7UUFDdkQsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFFO1FBQ2YsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFFO1FBQ2hCLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBRTtJQUNyQixDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUU7SUFFWCxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRTtRQUMxRCxJQUFJLEVBQUUsR0FBZ0IsUUFBUSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBRTtRQUMxRCxFQUFFLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBSSxFQUFFLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO0lBQ2xGLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBRTtJQUVYLFFBQVEsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFO1FBQ3RELE9BQU8sQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFFO1FBQzdCLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBRTtRQUNoQixJQUFJLEVBQUUsR0FBZ0IsUUFBUSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBRTtRQUMxRCxFQUFFLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBSSxFQUFFLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFFO0lBQ25GLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBRTtJQUVYLFFBQVEsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFO1FBQzlELE9BQU8sQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUU7UUFDMUQsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFFO0lBQ3BCLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBRTtJQUVYLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFO1FBQzFELElBQUksRUFBRSxHQUFnQixRQUFRLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFFO1FBQ3hELEVBQUUsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsVUFBVSxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7SUFDbEYsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFFO0lBRVgsRUFBRTtJQUNGLGtCQUFrQjtJQUNsQixFQUFFO0lBRUQseUJBQXlCO0lBQ3pCLFFBQVEsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFO1FBQ3ZELEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNiLFFBQVEsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUU7SUFDcEUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFFO0lBRVgsSUFBSSxLQUFLLEdBQVEsU0FBUyxDQUFFO0lBQzVCLElBQUksVUFBVSxHQUFXLEdBQUcsQ0FBRTtJQUM5QixRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRTtRQUVyRCxtQkFBbUI7UUFDbkIsSUFBSSxFQUFFLEdBQXNCLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUU7UUFDakUsSUFBSSxRQUFRLEdBQVcsVUFBVSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUU7UUFFcEQsMkNBQTJDO1FBQzNDLFFBQVEsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUMsV0FBVyxHQUFNLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFJLENBQUU7UUFDdkYsSUFBSSxPQUFPLEdBQVcsUUFBUSxDQUFFO1FBQ2hDLEtBQUssR0FBRyxXQUFXLENBQUU7WUFDakIsT0FBTyxJQUFJLFVBQVUsQ0FBRTtZQUN2QixFQUFFLENBQUEsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDZCxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUU7Z0JBQ2QsT0FBTyxHQUFHLFFBQVEsQ0FBRTtZQUN4QixDQUFDO1lBQ0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTLEdBQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQUksQ0FBRTtRQUN4RixDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUU7UUFFaEIsNEJBQTRCO1FBQzVCLFFBQVEsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUU7UUFDaEUsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFFO0lBQ2xCLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBRTtJQUVYLFFBQVEsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFO1FBQ3RELE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUU7UUFDNUIsRUFBRSxDQUFBLENBQUMsS0FBSyxDQUFDO1lBQ0wsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFFO1FBRTFCLEtBQUssR0FBRyxTQUFTLENBQUU7SUFDdkIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFFO0lBRVgsRUFBRTtJQUNGLDBCQUEwQjtJQUMxQixFQUFFO0lBRUYsdUJBQXVCO0lBQ3ZCLFFBQVEsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLFVBQUMsR0FBTztRQUNyRSxFQUFFLENBQUEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztZQUNsQixLQUFLLENBQUMsUUFBUSxHQUFHLGlCQUFTLENBQUMsS0FBSyxDQUFFO0lBQzFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBRTtJQUVYLFFBQVEsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLFVBQUMsR0FBTztRQUNyRSxFQUFFLENBQUEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztZQUNsQixLQUFLLENBQUMsUUFBUSxHQUFHLGlCQUFTLENBQUMsS0FBSyxDQUFFO0lBQzFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBRTtJQUVYLFFBQVEsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLFVBQUMsR0FBTztRQUNyRSxFQUFFLENBQUEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztZQUNsQixLQUFLLENBQUMsUUFBUSxHQUFHLGlCQUFTLENBQUMsS0FBSyxDQUFFO0lBQzFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBRTtJQUVYLHlCQUF5QjtJQUN6QixRQUFRLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxVQUFDLEdBQU87UUFFaEUsMkJBQTJCO1FBQzNCLElBQUksT0FBTyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFFO1FBQ25DLElBQUksTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUU7UUFDL0IsTUFBTSxDQUFDLE1BQU0sR0FBRztZQUNaLG9CQUFvQjtZQUNwQixJQUFJLEdBQUcsR0FBVyxNQUFNLENBQUMsTUFBTSxDQUFFO1lBRWpDLHdDQUF3QztZQUN4QyxJQUFNLEdBQUcsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBbUIsVUFBQyxJQUFJO2dCQUNuRCxJQUFJLEVBQUUsR0FBa0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBRTtnQkFDMUMsRUFBRSxDQUFBLENBQUMsRUFBRSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUM7b0JBQ2QsTUFBTSxDQUFFO2dCQUNaLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBRTtZQUMzQixDQUFDLENBQUMsQ0FBRTtZQUVKLDJCQUEyQjtZQUMzQixLQUFLLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsVUFBQSxFQUFFLElBQUksT0FBQSxFQUFFLElBQUksU0FBUyxFQUFmLENBQWUsQ0FBQyxDQUFFO1lBQ25ELE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBRTtZQUNqQixNQUFNLENBQUMsS0FBSyxFQUFFLENBQUU7UUFDcEIsQ0FBQyxDQUFBO1FBQ0QsTUFBTSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBRTtJQUNoQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUU7SUFFWCxFQUFFO0lBQ0Ysa0JBQWtCO0lBQ2xCLEVBQUU7SUFFRixRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRTtRQUUxRCxHQUFHO1FBQ0gsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFxQixRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBRSxDQUFDLEtBQUssQ0FBQyxDQUFFO1FBQ3hGLElBQUksTUFBTSxHQUFHLFFBQVEsQ0FBcUIsUUFBUSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBRTtRQUMxRixJQUFJLE1BQU0sR0FBRyxRQUFRLENBQXFCLFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFFLENBQUMsS0FBSyxDQUFDLENBQUU7UUFFMUYsd0JBQXdCO1FBQ3hCLElBQUksS0FBSyxHQUFnQyxJQUFJLEtBQUssRUFBd0IsQ0FBRTtRQUM1RSxHQUFHLENBQUEsQ0FBQyxJQUFJLEdBQUcsR0FBQyxDQUFDLEVBQUUsR0FBRyxHQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBQyxDQUFDO1lBQzlCLElBQUksSUFBSSxHQUF5QixLQUFLLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBRTtZQUNsRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFFO1FBQ3RCLENBQUM7UUFFRCwwQkFBMEI7UUFDMUIsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBRTtJQUM5QixDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUU7SUFHWCxRQUFRLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRTtRQUMvRCxnREFBZ0Q7UUFDaEQsSUFBSSxHQUFHLEdBQ0gsU0FBUztZQUNULDZFQUE2RTtZQUM3RSx3Q0FBd0M7WUFDeEMsa0ZBQWtGO1lBQ2xGLFVBQVUsQ0FBRTtRQUNoQixJQUFJLElBQUksR0FBRyw0QkFBNEIsR0FBRyxZQUFZLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUU7UUFDakYsSUFBSSxJQUFJLEdBQVMsSUFBSSxJQUFJLENBQUMsQ0FBQyxRQUFRLEVBQUUsR0FBRyxHQUFHLElBQUksQ0FBQyxFQUFFO1lBQzlDLElBQUksRUFBRSxvQkFBb0I7U0FDN0IsQ0FBQyxDQUFDO1FBRUgsaUJBQWlCO1FBQ2pCLEVBQUUsQ0FBQSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFBLENBQUM7WUFDM0IsU0FBUyxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxpQkFBaUIsQ0FBQyxDQUFFO1lBQ3JELDZCQUE2QjtRQUNqQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLEdBQUcsR0FBVyxHQUFHLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFFO1lBQzdDLElBQUksSUFBSSxHQUFxQyxRQUFRLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFFO1lBQzFFLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFFO1lBQ2pCLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFFO1lBQ2pDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBRTtZQUNkLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFFO1FBQ3JDLENBQUM7SUFDTCxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUU7QUFDZixDQUFDO0FBRUQsc0JBQXNCO0FBQ3RCLFVBQVUsRUFBRSxDQUFFOzs7QUNoTWQ7OztHQUdHOztBQUVILCtDQUErQztBQUMvQztJQUdJLHVCQUFZLEdBQWdCO1FBQ3hCLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFFO0lBQ3BCLENBQUM7SUFFRCw0QkFBSSxHQUFKLFVBQUssS0FBYTtRQUNkLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBRTtJQUNoQyxDQUFDO0lBRUQsNkJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLFlBQVksQ0FBRTtJQUN2QyxDQUFDO0lBQ0wsb0JBQUM7QUFBRCxDQWRBLEFBY0MsSUFBQTtBQWRZLHNDQUFhO0FBZ0IxQiw4QkFBOEI7QUFDOUI7SUFLSSx3QkFBWSxHQUFnQjtRQUN4QixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBRTtRQUNoQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssRUFBaUIsQ0FBRTtRQUN2QyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBRTtJQUMvQixDQUFDO0lBRUQsc0JBQUksdUNBQVc7YUFJZjtZQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFFO1FBQzlCLENBQUM7YUFORCxVQUFnQixHQUFZO1lBQ3hCLElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFFO1FBQzdCLENBQUM7OztPQUFBO0lBS0QsNEJBQUcsR0FBSCxVQUFJLEtBQWEsRUFBRSxNQUFjO1FBQzdCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUU7SUFDeEMsQ0FBQztJQUVELDZCQUFJLEdBQUo7UUFDSSxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLG1CQUFtQixDQUFFO1lBQzFDLE1BQU0sQ0FBRTtRQUNaLENBQUM7UUFFRCxlQUFlO1FBQ2YsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDbEIsSUFBSSxJQUFJLEdBQVcsMERBQTBELENBQUU7WUFDL0UsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQVEsQ0FBQyxFQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQyxDQUFDO2dCQUMzQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFFO2dCQUN6QixJQUFJLElBQUksY0FBVyxDQUFDLEdBQUcsQ0FBQyxrQkFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBWSxDQUFFO1lBQzFFLENBQUM7WUFDRCxJQUFJLElBQUksVUFBVSxDQUFFO1lBQ3BCLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBRTtZQUMvQixrQkFBa0I7UUFDbEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxJQUFJLEdBQVcsMENBQTBDLENBQUU7WUFDL0QsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQVEsQ0FBQyxFQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQyxDQUFDO2dCQUMzQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFFO2dCQUN6QixJQUFJLElBQUksY0FBVyxDQUFDLEdBQUcsQ0FBQyxrQkFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQVksQ0FBRTtZQUMxRCxDQUFDO1lBQ0QsSUFBSSxJQUFJLFVBQVUsQ0FBRTtZQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUU7UUFDL0IsQ0FBQztJQUVMLENBQUM7SUFFRCw4QkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLEVBQWlCLENBQUU7UUFDdkMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFFO0lBQzdCLENBQUM7SUFDTCxxQkFBQztBQUFELENBdERBLEFBc0RDLElBQUE7QUF0RFksd0NBQWM7QUF3RDNCLHNCQUFzQjtBQUN0QjtJQUlJLHNCQUFZLEdBQWdCO1FBQ3hCLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFFO0lBQ3BCLENBQUM7SUFFRCw0QkFBSyxHQUFMLFVBQU0sSUFBMEI7UUFDNUIsSUFBSSxJQUFJLEdBQUcsU0FBUyxDQUFFO1FBQ3RCLEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQzlCLElBQUksSUFBSSxNQUFNLENBQUU7WUFDaEIsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQ2pDLElBQUksSUFBSSxTQUFPLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBTyxDQUFFO1lBQ3RDLENBQUM7WUFDRCxJQUFJLElBQUksT0FBTyxDQUFFO1FBQ3JCLENBQUM7UUFDRCxJQUFJLElBQUksVUFBVSxDQUFFO1FBQ3BCLE1BQU0sQ0FBQyxJQUFJLENBQUU7SUFDakIsQ0FBQztJQUVELDJCQUFJLEdBQUosVUFBSyxLQUFrQztRQUNuQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUU7UUFDekIsR0FBRyxDQUFBLENBQUMsSUFBSSxHQUFHLEdBQUMsQ0FBQyxFQUFFLEdBQUcsR0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxFQUFDLENBQUM7WUFDcEMsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFFO1lBQ3ZCLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUU7WUFDeEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFFO1FBQ3BDLENBQUM7SUFDTCxDQUFDO0lBQ0wsbUJBQUM7QUFBRCxDQTdCQSxBQTZCQyxJQUFBO0FBN0JZLG9DQUFZIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIlxyXG4vKipcclxuICogVGhlIGNsYXNzZXMgdG8gY3JlYXRlIGEgQmluZ28gZ2FtZSBcclxuICovXHJcbmV4cG9ydCBlbnVtIEdhbWVNb2RlcyB7bWl4ZWQsIHNob3dBLCBzaG93Qn0gO1xyXG5cclxuLy8gdGhlIEJpbmdvIGFwcFxyXG5leHBvcnQgY2xhc3MgQmluZ29HYW1lIHtcclxuICAgIFxyXG4gICAgLy8gXHJcbiAgICBidXN5OiBib29sZWFuIDtcclxuXHJcbiAgICBjdXJyZW50OiAoeDogc3RyaW5nKSA9PiB2b2lkIDtcclxuICAgIHBhc3Q6IChzaG93bjogc3RyaW5nLCBvdGhlcjogc3RyaW5nKSAgPT4gdm9pZDtcclxuXHJcbiAgICBfZ2FtZV9tb2RlOiBHYW1lTW9kZXMgO1xyXG4gICAgX2NvbnRlbnQ6IEFycmF5PFtzdHJpbmcsIHN0cmluZ10+IDtcclxuICAgIF91c2VkOiBBcnJheTxbc3RyaW5nLCBzdHJpbmddPiA7XHJcblxyXG4gICAgLy8gVGhlIGNvbnN0cnVjdG9yXHJcbiAgICBjb25zdHJ1Y3RvcihjdXJyZW50OiAgKHg6IHN0cmluZykgPT4gdm9pZCwgcGFzdDogKHNob3duOiBzdHJpbmcsIG90aGVyOiBzdHJpbmcpICA9PiB2b2lkKSB7XHJcbiAgICAgICAgdGhpcy5idXN5ID0gZmFsc2UgO1xyXG4gICAgICAgIHRoaXMuY3VycmVudCA9IGN1cnJlbnQgO1xyXG4gICAgICAgIHRoaXMucGFzdCA9IHBhc3QgO1xyXG5cclxuICAgICAgICB0aGlzLl9nYW1lX21vZGUgPSBHYW1lTW9kZXMubWl4ZWQgO1xyXG4gICAgICAgIHRoaXMuX2NvbnRlbnQgPSBuZXcgQXJyYXk8W3N0cmluZywgc3RyaW5nXT4oKSA7XHJcbiAgICAgICAgdGhpcy5fdXNlZCA9IG5ldyBBcnJheTxbc3RyaW5nLCBzdHJpbmddPigpIDtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgZ2FtZU1vZGUoZ206IEdhbWVNb2Rlcykge1xyXG4gICAgICAgIHRoaXMuX2dhbWVfbW9kZSA9IGdtIDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZ2FtZU1vZGUoKTogR2FtZU1vZGVzIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZ2FtZV9tb2RlIDtcclxuICAgIH1cclxuXHJcbiAgICAvLyAoc2hhbGxvdykgY29weSB0aGUgY29udGVudFxyXG4gICAgc2V0IGNvbnRlbnQoY246IEFycmF5PFtzdHJpbmcsIHN0cmluZ10+KSB7XHJcbiAgICAgICAgdGhpcy5fY29udGVudCA9IGNuLnNsaWNlKCkgO1xyXG4gICAgICAgIHRoaXMuX3VzZWQgPSBuZXcgQXJyYXk8W3N0cmluZywgc3RyaW5nXT4oKSA7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGNvbnRlbnQoKTogQXJyYXk8W3N0cmluZywgc3RyaW5nXT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9jb250ZW50IDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBkbyB0aGUgbmV4dCBkcmF3XHJcbiAgICBuZXh0KCkgOiBudW1iZXIge1xyXG4gICAgICAgIGlmKHRoaXMuYnVzeSl7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiYWxyZWFkeSBydW5uaW5nIG5leHQgb3IgcmVzZXRcIikgO1xyXG4gICAgICAgICAgICByZXR1cm4gLTEgO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmJ1c3kgPSB0cnVlIDtcclxuICAgICAgICBsZXQgaW5kZXggPSB0aGlzLl9uZXh0KCkgO1xyXG4gICAgICAgIHRoaXMuYnVzeSA9IGZhbHNlIDtcclxuICAgICAgICBpZihpbmRleCA9PSAtMSl7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudChcIkdhbWUgb3ZlclwiKSA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpbmRleCA7XHJcbiAgICB9XHJcbiAgICAgICAgXHJcbiAgICAvLyByZXNldCB0aGUgZ2FtZVxyXG4gICAgcmVzZXQoKSB7ICAgICAgICBcclxuICAgICAgICBpZih0aGlzLmJ1c3kpe1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcImFscmVhZHkgcnVubmluZyBuZXh0IG9yIHJlc2V0XCIpIDtcclxuICAgICAgICAgICAgcmV0dXJuIDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5idXN5ID0gdHJ1ZSA7XHJcbiAgICAgICAgdGhpcy5fcmVzZXQoKSA7XHJcbiAgICAgICAgdGhpcy5idXN5ID0gZmFsc2UgO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGNyZWF0ZSBhIG5ldyBiaW5nbyBjYXJkXHJcbiAgICBjcmVhdGVDYXJkKHJvd3M6IG51bWJlciwgY29sdW1uczogbnVtYmVyKTogQXJyYXk8QXJyYXk8c3RyaW5nPj4ge1xyXG4gICAgICAgIGxldCByZXR2YWw6IEFycmF5PEFycmF5PHN0cmluZz4+ID0gW10gO1xyXG5cclxuICAgICAgICAvLyByZWFsbHkgaW5lZmZpY2llbnQsIGJ1dCB0aGUgc2ltcGxlc3Qgd2F5IHRvIG1ha2Ugc3VyZSB3ZSBkb24ndCBzZWxlY3QgZG91Ymxlc1xyXG4gICAgICAgIGxldCBjb250ZW50ID0gdGhpcy5fY29udGVudC5zbGljZSgpIDtcclxuXHJcbiAgICAgICAgLy8gZm9yZWFjaCByb3cgYWRkIGEgbmV3IHJvdyB0byB0aGUgb3V0cHV0XHJcbiAgICAgICAgZm9yKGxldCByPTA7IHI8cm93czsgcisrKXtcclxuICAgICAgICAgICAgcmV0dmFsLnB1c2goW10pIDtcclxuXHJcbiAgICAgICAgICAgIC8vIHBpY2sgYSBsYWJlbCBmcm9tIHRoZSBjb250ZW50XHJcbiAgICAgICAgICAgIGZvcihsZXQgYz0wOyBjPGNvbHVtbnM7IGMrKyl7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gbm8gbW9yZSBlbGVtZW50cyBsZWZ0IHRvIGNob3NlIGZyb21cclxuICAgICAgICAgICAgICAgIGlmKGNvbnRlbnQubGVuZ3RoID09IDApe1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHZhbFtyXS5wdXNoKFwiXCIpIDtcclxuICAgICAgICAgICAgICAgICAgICBjb250aW51ZSA7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IGlkeDogbnVtYmVyID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogY29udGVudC5sZW5ndGgpIDtcclxuICAgICAgICAgICAgICAgIGxldCB2YWx1ZTogW3N0cmluZywgc3RyaW5nXSA9IGNvbnRlbnQuc3BsaWNlKGlkeCwgMSlbMF0gO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIHNlbGVjdCB0aGUgbGFiZWxcclxuICAgICAgICAgICAgICAgIGxldCBsYWJlbCA9IFwiXCIgO1xyXG4gICAgICAgICAgICAgICAgc3dpdGNoKHRoaXMuX2dhbWVfbW9kZSl7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBHYW1lTW9kZXMubWl4ZWQ6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsID0gdmFsdWVbTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogMildIDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWsgO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgR2FtZU1vZGVzLnNob3dBOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbCA9IHZhbHVlWzFdIDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWsgO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgR2FtZU1vZGVzLnNob3dCOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbCA9IHZhbHVlWzBdIDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWsgO1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrIDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIGFkZCB0aGUgbGFiZWwgdG8gdGhlIHJldHVybiB2YWx1ZVxyXG4gICAgICAgICAgICAgICAgcmV0dmFsW3JdLnB1c2gobGFiZWwpIDtcclxuICAgICAgICAgICAgfSAgICBcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuKHJldHZhbCkgO1xyXG4gICAgfVxyXG5cclxuICAgIC8vXHJcbiAgICAvLyBQcml2YXRlIGZ1bmN0aW9uc1xyXG4gICAgLy9cclxuXHJcbiAgICBwcml2YXRlIF9uZXh0KCkge1xyXG5cclxuICAgICAgICAvLyBnYW1lIG92ZXIgb3V0IG9mIGVsZW1lbnRzXHJcbiAgICAgICAgaWYodGhpcy5fY29udGVudC5sZW5ndGggPT0gMCl7XHJcbiAgICAgICAgICAgIHJldHVybiAtMVxyXG4gICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgIC8vIGNob29zZSB0aGUgbmV4dCB2YWx1ZSB0byBnZXQgYW5kIHJlbW92ZSBpdCBmcm9tIHRoZSBjb250ZW50XHJcbiAgICAgICAgbGV0IGluZGV4OiBudW1iZXIgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiB0aGlzLl9jb250ZW50Lmxlbmd0aCkgO1xyXG4gICAgICAgIGxldCB2YWx1ZTogW3N0cmluZywgc3RyaW5nXSA9IHRoaXMuX2NvbnRlbnQuc3BsaWNlKGluZGV4LCAxKVswXSA7XHJcblxyXG4gICAgICAgIC8vIGZpcnN0IGFkZCBpdCB0byB0aGUgYWxyZWFkeSB1c2VkIGRhdGFcclxuICAgICAgICB0aGlzLl91c2VkLnB1c2godmFsdWUpIDtcclxuXHJcbiAgICAgICAgLy8gR2V0IHRoZWRpc3BsYXkgbGFiZWxcclxuICAgICAgICBsZXQgbGFiZWw6IHN0cmluZyA9IFwiXCIgO1xyXG4gICAgICAgIGxldCBvdGhlcjogc3RyaW5nID0gXCJcIiA7XHJcbiAgICAgICAgc3dpdGNoKHRoaXMuX2dhbWVfbW9kZSl7XHJcbiAgICAgICAgICAgIGNhc2UgR2FtZU1vZGVzLm1peGVkOlxyXG4gICAgICAgICAgICAgICAgbGV0IGk6IG51bWJlciA9IE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIDIpIDtcclxuICAgICAgICAgICAgICAgIGxhYmVsID0gdmFsdWVbaV0gO1xyXG4gICAgICAgICAgICAgICAgb3RoZXIgPSBpID09IDEgPyB2YWx1ZVswXSA6IHZhbHVlWzFdIDtcclxuICAgICAgICAgICAgICAgIGJyZWFrIDtcclxuICAgICAgICAgICAgY2FzZSBHYW1lTW9kZXMuc2hvd0E6XHJcbiAgICAgICAgICAgICAgICBsYWJlbCA9IHZhbHVlWzBdIDtcclxuICAgICAgICAgICAgICAgIG90aGVyID0gdmFsdWVbMV0gO1xyXG4gICAgICAgICAgICAgICAgYnJlYWsgO1xyXG4gICAgICAgICAgICBjYXNlIEdhbWVNb2Rlcy5zaG93QjpcclxuICAgICAgICAgICAgICAgIG90aGVyID0gdmFsdWVbMF0gO1xyXG4gICAgICAgICAgICAgICAgbGFiZWwgPSB2YWx1ZVsxXSA7XHJcbiAgICAgICAgICAgICAgICBicmVhayA7XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICBicmVhayA7XHJcbiAgICAgICAgfSBcclxuXHJcbiAgICAgICAgLy8gU2hvdyB0aGUgZGlzcGxheSBsYWJlbCBhbmQgYWRkIGl0IHRvIHRoZSBsaXN0XHJcbiAgICAgICAgdGhpcy5jdXJyZW50KGxhYmVsKSA7XHJcblxyXG4gICAgICAgIC8vIGFwcGVuZCB0aGUgdmFsdWUgdG8gdGhlIGxpc3RcclxuICAgICAgICB0aGlzLnBhc3QobGFiZWwsIG90aGVyKSA7XHJcblxyXG4gICAgICAgIC8vIHJldHVybiB0aGUgaW5kZXggb2YgdGhlIGN1cnJlbnQgcGlja1xyXG4gICAgICAgIHJldHVybiBpbmRleCA7ICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9yZXNldCgpe1xyXG4gICAgICAgIGZvcihsZXQgaT0wOyBpPHRoaXMuX3VzZWQubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICB0aGlzLl9jb250ZW50LnB1c2godGhpcy5fdXNlZFtpXSkgO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl91c2VkID0gbmV3IEFycmF5PFtzdHJpbmcsIHN0cmluZ10+KCkgO1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuIiwiXHJcbmltcG9ydCB7IEdhbWVNb2RlcywgQmluZ29HYW1lIH0gZnJvbSBcIi4vYmluZ29cIiA7XHJcbmltcG9ydCB7IExhYmVsUmVuZGVyZXIsIEFuc3dlclJlbmRlcmVyLCBDYXJkUmVuZGVyZXJ9IGZyb20gXCIuL3JlbmRlcmVyc1wiIDtcclxuXHJcbi8vIE1haW4gZ2FtZSBpbml0aWFsaXphdGlvblxyXG4vL1xyXG5mdW5jdGlvbiBpbml0aWFsaXplKCl7XHJcbiAgICAvLyBQcmVwYXJlIHRoZSByZW5kZXJlcnNcclxuICAgIGxldCBjYXJkcmVuZGVyZXIgPSBuZXcgQ2FyZFJlbmRlcmVyKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY2FyZGhvbGRlclwiKSkgO1xyXG4gICAgbGV0IHBpY2tlZCA9IG5ldyBMYWJlbFJlbmRlcmVyKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY3VycmVudF9waWNrXCIpKSA7XHJcbiAgICBsZXQgYW5zd2VycyA9IG5ldyBBbnN3ZXJSZW5kZXJlcihkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImFuc3dlcl90YWJsZVwiKSkgO1xyXG5cclxuICAgIC8vIFNldHVwIHRoZSBCaW5nbyBnYW1lXHJcbiAgICBsZXQgYmluZ28gPSBuZXcgQmluZ29HYW1lKFxyXG4gICAgICAgIChsYWJlbDogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIHBpY2tlZC5zaG93KGxhYmVsKSA7XHJcbiAgICAgICAgfSxcclxuICAgICAgICAobGFiZWw6IHN0cmluZywgb3RoZXI6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICBhbnN3ZXJzLmFkZChsYWJlbCwgb3RoZXIpO1xyXG4gICAgICAgIH1cclxuICAgICkgO1xyXG5cclxuXHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInJlc2V0XCIpLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCAoKSA9PiB7XHJcbiAgICAgICAgYmluZ28ucmVzZXQoKSA7XHJcbiAgICAgICAgcGlja2VkLnJlc2V0KCkgO1xyXG4gICAgICAgIGFuc3dlcnMucmVzZXQoKSA7XHJcbiAgICB9LCBmYWxzZSkgO1xyXG5cclxuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic2V0dGluZ3NcIikuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsICgpID0+IHtcclxuICAgICAgICBsZXQgZWw6IEhUTUxFbGVtZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJvcHRpb25zXCIpIDtcclxuICAgICAgICBlbC5zdHlsZS52aXNpYmlsaXR5ID0gIGVsLnN0eWxlLnZpc2liaWxpdHkgPT0gXCJoaWRkZW5cIiA/IFwidmlzaWJsZVwiIDogXCJoaWRkZW5cIjtcclxuICAgIH0sIGZhbHNlKSA7XHJcblxyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJsaXN0XCIpLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCAoKSA9PiB7XHJcbiAgICAgICAgYW5zd2Vycy5zaG93QW5zd2VycyA9IGZhbHNlIDtcclxuICAgICAgICBhbnN3ZXJzLnNob3coKSA7XHJcbiAgICAgICAgbGV0IGVsOiBIVE1MRWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYW5zd2Vyc1wiKSA7XHJcbiAgICAgICAgZWwuc3R5bGUudmlzaWJpbGl0eSA9ICBlbC5zdHlsZS52aXNpYmlsaXR5ID09IFwiaGlkZGVuXCIgPyBcInZpc2libGVcIiA6IFwiaGlkZGVuXCIgO1xyXG4gICAgfSwgZmFsc2UpIDtcclxuXHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInNob3dfYW5zd2Vyc1wiKS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgKCkgPT4ge1xyXG4gICAgICAgIGFuc3dlcnMuc2hvd0Fuc3dlcnMgPSBhbnN3ZXJzLnNob3dBbnN3ZXJzID8gZmFsc2UgOiB0cnVlIDtcclxuICAgICAgICBhbnN3ZXJzLnNob3coKSA7XHJcbiAgICB9LCBmYWxzZSkgO1xyXG5cclxuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibWFrZWNhcmRcIikuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsICgpID0+IHtcclxuICAgICAgICBsZXQgZWw6IEhUTUxFbGVtZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjYXJkc1wiKSA7XHJcbiAgICAgICAgZWwuc3R5bGUudmlzaWJpbGl0eSA9ICBlbC5zdHlsZS52aXNpYmlsaXR5ID09IFwiaGlkZGVuXCIgPyBcInZpc2libGVcIiA6IFwiaGlkZGVuXCI7XHJcbiAgICB9LCBmYWxzZSkgO1xyXG5cclxuICAgIC8vXHJcbiAgICAvLyBUaW1lciBmdW5jdGlvbnNcclxuICAgIC8vXHJcbiAgICBcclxuICAgICAvLyBBZGQgdGhlIGJpbmdvIGNvbnRyb2xzXHJcbiAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJuZXh0XCIpLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCAoKSA9PiB7XHJcbiAgICAgICAgYmluZ28ubmV4dCgpO1xyXG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwib3B0aW9uc1wiKS5zdHlsZS52aXNpYmlsaXR5ID0gXCJoaWRkZW5cIiA7XHJcbiAgICB9LCBmYWxzZSkgO1xyXG5cclxuICAgIGxldCB0aW1lcjogYW55ID0gdW5kZWZpbmVkIDtcclxuICAgIGxldCB1cGRhdGV0aW1lOiBudW1iZXIgPSA1MDAgO1xyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJydW5cIikuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsICgpID0+IHtcclxuXHJcbiAgICAgICAgLy8gZ2V0IHRoZSBpbnRlcnZhbFxyXG4gICAgICAgIGxldCBlbCA9IDxIVE1MSW5wdXRFbGVtZW50PiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImludGVydmFsXCIpIDtcclxuICAgICAgICBsZXQgdGltZXNwYW46IG51bWJlciA9IHBhcnNlRmxvYXQoZWwudmFsdWUpICogMTAwMCA7XHJcblxyXG4gICAgICAgIC8vIGNhbGwgYmluZ28gbmV4dCB3aGVuIHRoZSB0aW1lciByZWFjaGVzIDBcclxuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNvdW50ZG93blwiKS50ZXh0Q29udGVudCA9IGAke01hdGgucm91bmQodGltZXNwYW4gLyAxMDAwKX0gc2AgO1xyXG4gICAgICAgIGxldCBjdXJ0aW1lOiBudW1iZXIgPSB0aW1lc3BhbiA7XHJcbiAgICAgICAgdGltZXIgPSBzZXRJbnRlcnZhbCggKCk9PiB7XHJcbiAgICAgICAgICAgIGN1cnRpbWUgLT0gdXBkYXRldGltZSA7XHJcbiAgICAgICAgICAgIGlmKGN1cnRpbWUgPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgYmluZ28ubmV4dCgpIDtcclxuICAgICAgICAgICAgICAgIGN1cnRpbWUgPSB0aW1lc3BhbiA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb3VudGRvd25cIikuaW5uZXJUZXh0ID0gYCR7TWF0aC5yb3VuZChjdXJ0aW1lIC8gMTAwMCl9IHNgIDtcclxuICAgICAgICB9LCB1cGRhdGV0aW1lKSA7XHJcblxyXG4gICAgICAgIC8vIHNldCB0aGUgb3B0aW9ucyB0byBoaWRkZW5cclxuICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm9wdGlvbnNcIikuc3R5bGUudmlzaWJpbGl0eSA9IFwiaGlkZGVuXCIgO1xyXG4gICAgICAgIGJpbmdvLm5leHQoKSA7XHJcbiAgICB9LCBmYWxzZSkgO1xyXG5cclxuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic3RvcFwiKS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgKCkgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiU3RvcCBjYWxsZWRcIikgO1xyXG4gICAgICAgIGlmKHRpbWVyKVxyXG4gICAgICAgICAgICBjbGVhckludGVydmFsKHRpbWVyKSA7XHJcblxyXG4gICAgICAgIHRpbWVyID0gdW5kZWZpbmVkIDtcclxuICAgIH0sIGZhbHNlKSA7XHJcblxyXG4gICAgLy9cclxuICAgIC8vIEdhbWUgYWx0ZXJpbmcgZnVuY3Rpb25zXHJcbiAgICAvL1xyXG5cclxuICAgIC8vIHNlbGVjdCB0aGUgZ2FtZSBtb2RlXHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImdhbWV0eXBlXzFcIikuYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCAoZXZ0OmFueSkgPT4ge1xyXG4gICAgICAgIGlmKGV2dC50YXJnZXQuY2hlY2tlZClcclxuICAgICAgICAgICAgYmluZ28uZ2FtZU1vZGUgPSBHYW1lTW9kZXMubWl4ZWQgO1xyXG4gICAgfSwgZmFsc2UpIDtcclxuXHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImdhbWV0eXBlXzJcIikuYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCAoZXZ0OmFueSkgPT4ge1xyXG4gICAgICAgIGlmKGV2dC50YXJnZXQuY2hlY2tlZClcclxuICAgICAgICAgICAgYmluZ28uZ2FtZU1vZGUgPSBHYW1lTW9kZXMuc2hvd0EgO1xyXG4gICAgfSwgZmFsc2UpIDtcclxuXHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImdhbWV0eXBlXzNcIikuYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCAoZXZ0OmFueSkgPT4ge1xyXG4gICAgICAgIGlmKGV2dC50YXJnZXQuY2hlY2tlZClcclxuICAgICAgICAgICAgYmluZ28uZ2FtZU1vZGUgPSBHYW1lTW9kZXMuc2hvd0IgO1xyXG4gICAgfSwgZmFsc2UpIDtcclxuXHJcbiAgICAvLyBhZGQgdGhlIGNvbnRlbnQgbG9hZGVyXHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImZpbGVzXCIpLmFkZEV2ZW50TGlzdGVuZXIoXCJjaGFuZ2VcIiwgKGV2dDphbnkpID0+IHtcclxuXHJcbiAgICAgICAgLy8gb25seSBsb2FkIHRoZSBmaXJzdCBmaWxlXHJcbiAgICAgICAgbGV0IGN1cmZpbGUgPSBldnQudGFyZ2V0LmZpbGVzWzBdIDtcclxuICAgICAgICBsZXQgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKSA7XHJcbiAgICAgICAgcmVhZGVyLm9ubG9hZCA9ICgpID0+IHtcclxuICAgICAgICAgICAgLy8gZ2V0IHRoZSByYXcgdGV4dCBcclxuICAgICAgICAgICAgbGV0IHJhdzogc3RyaW5nID0gcmVhZGVyLnJlc3VsdCA7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAvLyBzcGxpdCB0aGUgc3RyaW5nIG9uIG5ld2xpbmVzIGFuZCB0YWJzXHJcbiAgICAgICAgICAgIGNvbnN0IHJlcyA9IHJhdy5zcGxpdChcIlxcblwiKS5tYXA8W3N0cmluZywgc3RyaW5nXT4oKGxpbmUpID0+IHtcclxuICAgICAgICAgICAgICAgIGxldCBlbDogQXJyYXk8c3RyaW5nPiA9IGxpbmUuc3BsaXQoXCJcXHRcIikgO1xyXG4gICAgICAgICAgICAgICAgaWYoZWwubGVuZ3RoICE9IDIpXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDtcclxuICAgICAgICAgICAgICAgIHJldHVybiBbZWxbMF0sIGVsWzFdXSA7XHJcbiAgICAgICAgICAgIH0pIDtcclxuXHJcbiAgICAgICAgICAgIC8vIGFkZCB0aGUgY29udGVudCB0byBiaW5nb1xyXG4gICAgICAgICAgICBiaW5nby5jb250ZW50ID0gcmVzLmZpbHRlcihlbCA9PiBlbCAhPSB1bmRlZmluZWQpIDtcclxuICAgICAgICAgICAgYW5zd2Vycy5yZXNldCgpIDtcclxuICAgICAgICAgICAgcGlja2VkLnJlc2V0KCkgO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZWFkZXIucmVhZEFzVGV4dChjdXJmaWxlKSA7XHJcbiAgICB9LCBmYWxzZSkgO1xyXG5cclxuICAgIC8vXHJcbiAgICAvLyBDYXJkIGdlbmVyYXRpb25cclxuICAgIC8vXHJcblxyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJnZW5lcmF0ZVwiKS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgKCkgPT4ge1xyXG5cclxuICAgICAgICAvLyBcclxuICAgICAgICBsZXQgd2lkdGggPSBwYXJzZUludCgoPEhUTUxJbnB1dEVsZW1lbnQ+IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY2FyZF93aWR0aFwiKSkudmFsdWUpIDtcclxuICAgICAgICBsZXQgaGVpZ2h0ID0gcGFyc2VJbnQoKDxIVE1MSW5wdXRFbGVtZW50PiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNhcmRfaGVpZ2h0XCIpKS52YWx1ZSkgO1xyXG4gICAgICAgIGxldCBudW1iZXIgPSBwYXJzZUludCgoPEhUTUxJbnB1dEVsZW1lbnQ+IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY2FyZF9udW1iZXJcIikpLnZhbHVlKSA7XHJcbiAgICAgICAgXHJcbiAgICAgICAgLy8gZ2VuZXJhdGUgbnVtYmVyIGNhcmRzXHJcbiAgICAgICAgbGV0IGNhcmRzOiBBcnJheTxBcnJheTxBcnJheTxzdHJpbmc+Pj4gPSBuZXcgQXJyYXk8QXJyYXk8QXJyYXk8c3RyaW5nPj4+KCkgO1xyXG4gICAgICAgIGZvcihsZXQgaWR4PTA7IGlkeDxudW1iZXI7IGlkeCsrKXtcclxuICAgICAgICAgICAgbGV0IGNhcmQ6IEFycmF5PEFycmF5PHN0cmluZz4+ID0gYmluZ28uY3JlYXRlQ2FyZChoZWlnaHQsIHdpZHRoKSA7XHJcbiAgICAgICAgICAgIGNhcmRzLnB1c2goY2FyZCkgO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gc2hvdyB0aGUgY2FyZHMgb25zY3JlZW5cclxuICAgICAgICBjYXJkcmVuZGVyZXIuc2hvdyhjYXJkcykgO1xyXG4gICAgfSwgZmFsc2UpIDtcclxuXHJcblxyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjYXJkX2Rvd25sb2FkXCIpLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCAoKSA9PiB7XHJcbiAgICAgICAgLy8gYWRhcHRlZCBmcm9tIGh0dHBzOi8vanNmaWRkbGUubmV0Lzc4eGExNHZ6LzMvXHJcbiAgICAgICAgbGV0IGNzczogc3RyaW5nID0gXHJcbiAgICAgICAgICAgIFwiPHN0eWxlPlwiICsgXHJcbiAgICAgICAgICAgICdAcGFnZSBXb3JkU2VjdGlvbjF7c2l6ZTogNTk1LjM1cHQgODQxLjk1cHQ7bXNvLXBhZ2Utb3JpZW50YXRpb246IHBvcnRyYWl0O30nICtcclxuICAgICAgICAgICAgJ2Rpdi5Xb3JkU2VjdGlvbjEge3BhZ2U6IFdvcmRTZWN0aW9uMTt9JyArXHJcbiAgICAgICAgICAgICd0YWJsZXtib3JkZXItY29sbGFwc2U6Y29sbGFwc2U7fXRke2JvcmRlcjoxcHggZ3JheSBzb2xpZDt3aWR0aDo1ZW07cGFkZGluZzoycHg7fScrXHJcbiAgICAgICAgICAgIFwiPC9zdHlsZT5cIiA7XHJcbiAgICAgICAgbGV0IGh0bWwgPSBcIjxkaXYgY2xhc3M9J1dvcmRTZWN0aW9uMSc+XCIgKyBjYXJkcmVuZGVyZXIudGFnLmlubmVySFRNTCArIFwiPC9kaXY+XCIgO1xyXG4gICAgICAgIGxldCBibG9iOiBCbG9iID0gbmV3IEJsb2IoWydcXHVmZWZmJywgY3NzICsgaHRtbF0sIHtcclxuICAgICAgICAgICAgdHlwZTogJ2FwcGxpY2F0aW9uL21zd29yZCdcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gZG93bmxvYWQgaW4gSUVcclxuICAgICAgICBpZihuYXZpZ2F0b3IubXNTYXZlT3JPcGVuQmxvYil7XHJcbiAgICAgICAgICAgIG5hdmlnYXRvci5tc1NhdmVPck9wZW5CbG9iKGJsb2IsIFwiYmluZ29fY2FyZHMuZG9jXCIpIDtcclxuICAgICAgICAgICAgLy8gZG93bmxvYWQgaW4gb3RoZXIgYnJvd3NlcnNcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBsZXQgdXJsOiBzdHJpbmcgPSBVUkwuY3JlYXRlT2JqZWN0VVJMKGJsb2IpIDtcclxuICAgICAgICAgICAgbGV0IGxpbms6IEhUTUxMaW5rRWxlbWVudCA9IDxIVE1MTGlua0VsZW1lbnQ+ZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnQScpIDtcclxuICAgICAgICAgICAgbGluay5ocmVmID0gdXJsIDtcclxuICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChsaW5rKSA7XHJcbiAgICAgICAgICAgIGxpbmsuY2xpY2soKSA7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQobGluaykgO1xyXG4gICAgICAgIH1cclxuICAgIH0sIGZhbHNlKSA7XHJcbn1cclxuXHJcbi8vIGluaXRpYWxpemUgdGhlIGdhbWVcclxuaW5pdGlhbGl6ZSgpIDtcclxuXHJcbiIsIi8qKlxyXG4gKiBSZW5kZXJlcnMgdG8gc2hvdyBkeW5hb21pYyBjb250ZW50IGZyb20gdGhlIEJpbmdvIGdhbWVcclxuICogXHJcbiAqL1xyXG5cclxuLy8gQSBjbGFzcyB0byByZW5kZXIgbGFiZWxzIG9uIHRoZSBtYWluIHNjcmVlbi5cclxuZXhwb3J0IGNsYXNzIExhYmVsUmVuZGVyZXIge1xyXG4gICAgdGFnOiBIVE1MRWxlbWVudCA7XHJcblxyXG4gICAgY29uc3RydWN0b3IodGFnOiBIVE1MRWxlbWVudCkge1xyXG4gICAgICAgIHRoaXMudGFnID0gdGFnIDtcclxuICAgIH1cclxuXHJcbiAgICBzaG93KGxhYmVsOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLnRhZy5pbm5lckhUTUwgPSBsYWJlbCA7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXQoKSB7XHJcbiAgICAgICAgdGhpcy50YWcuaW5uZXJIVE1MID0gXCJDbGljayBuZXh0XCIgO1xyXG4gICAgfVxyXG59XHJcblxyXG4vLyBhIGNsYXNzIHRvIHNob3cgdGhlIGFuc3dlcnNcclxuZXhwb3J0IGNsYXNzIEFuc3dlclJlbmRlcmVyIHtcclxuICAgIHRhZzogSFRNTEVsZW1lbnQgO1xyXG4gICAgYW5zd2VyczogQXJyYXk8QXJyYXk8c3RyaW5nPj4gO1xyXG4gICAgX3Nob3dBbnN3ZXJzOiBib29sZWFuIDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcih0YWc6IEhUTUxFbGVtZW50KSB7XHJcbiAgICAgICAgdGhpcy50YWcgPSB0YWcgO1xyXG4gICAgICAgIHRoaXMuYW5zd2VycyA9IEFycmF5PEFycmF5PHN0cmluZz4+KCkgO1xyXG4gICAgICAgIHRoaXMuX3Nob3dBbnN3ZXJzID0gZmFsc2UgO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBzaG93QW5zd2Vycyh2YWw6IGJvb2xlYW4pIHtcclxuICAgICAgICB0aGlzLl9zaG93QW5zd2VycyA9IHZhbCA7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHNob3dBbnN3ZXJzKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zaG93QW5zd2VycyA7XHJcbiAgICB9XHJcbiAgICBhZGQobGFiZWw6IHN0cmluZywgYW5zd2VyOiBzdHJpbmcpe1xyXG4gICAgICAgIHRoaXMuYW5zd2Vycy5wdXNoKFtsYWJlbCwgYW5zd2VyXSkgO1xyXG4gICAgfVxyXG5cclxuICAgIHNob3coKSB7XHJcbiAgICAgICAgaWYodGhpcy5hbnN3ZXJzLmxlbmd0aCA9PSAwKXtcclxuICAgICAgICAgICAgdGhpcy50YWcuaW5uZXJIVE1MID0gXCJObyBwcmV2aW91cyBwaWNrc1wiIDtcclxuICAgICAgICAgICAgcmV0dXJuIDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHdpdGggYW5zd2Vyc1xyXG4gICAgICAgIGlmKHRoaXMuc2hvd0Fuc3dlcnMpIHtcclxuICAgICAgICAgICAgbGV0IHRleHQ6IHN0cmluZyA9IFwiPHRhYmxlPjx0cj48dGg+IzwvdGg+PHRoPlNob3duPC90aD48dGg+U3lub255bTwvdGg+PC90cj5cIiA7XHJcbiAgICAgICAgICAgIGZvcihsZXQgaTpudW1iZXI9MDtpPHRoaXMuYW5zd2Vycy5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICAgICAgICBsZXQgeCA9IHRoaXMuYW5zd2Vyc1tpXSA7XHJcbiAgICAgICAgICAgICAgICB0ZXh0ICs9IGA8dHI+PHRkPiR7aSArIDF9PC90ZD48dGQ+JHt4WzBdfTwvdGQ+PHRkPiR7eFsxXX08L3RkPjwvdHI+YCA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGV4dCArPSBcIjwvdGFibGU+XCIgO1xyXG4gICAgICAgICAgICB0aGlzLnRhZy5pbm5lckhUTUwgPSB0ZXh0IDtcclxuICAgICAgICAvLyB3aXRob3V0IGFuc3dlcnNcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBsZXQgdGV4dDogc3RyaW5nID0gXCI8dGFibGU+PHRyPjx0aD4jPC90aD48dGg+U2hvd248L3RoPjwvdHI+XCIgO1xyXG4gICAgICAgICAgICBmb3IobGV0IGk6bnVtYmVyPTA7aTx0aGlzLmFuc3dlcnMubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgbGV0IHggPSB0aGlzLmFuc3dlcnNbaV0gO1xyXG4gICAgICAgICAgICAgICAgdGV4dCArPSBgPHRyPjx0ZD4ke2kgKyAxfTwvdGQ+PHRkPiR7eFswXX08L3RkPjwvdHI+YCA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGV4dCArPSBcIjwvdGFibGU+XCIgO1xyXG4gICAgICAgICAgICB0aGlzLnRhZy5pbm5lckhUTUwgPSB0ZXh0IDtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXQoKXtcclxuICAgICAgICB0aGlzLmFuc3dlcnMgPSBBcnJheTxBcnJheTxzdHJpbmc+PigpIDtcclxuICAgICAgICB0aGlzLnRhZy5pbm5lckhUTUwgPSBcIlwiIDtcclxuICAgIH1cclxufVxyXG5cclxuLy8gUmVuZGVyIGEgYmluZ28gY2FyZFxyXG5leHBvcnQgY2xhc3MgQ2FyZFJlbmRlcmVyIHtcclxuICAgIHRhZzogSFRNTEVsZW1lbnQgO1xyXG4gICAgY2FyZDogQXJyYXk8QXJyYXk8c3RyaW5nPj4gO1xyXG4gICAgXHJcbiAgICBjb25zdHJ1Y3Rvcih0YWc6IEhUTUxFbGVtZW50KSB7XHJcbiAgICAgICAgdGhpcy50YWcgPSB0YWcgO1xyXG4gICAgfVxyXG5cclxuICAgIHRhYmxlKGNhcmQ6IEFycmF5PEFycmF5PHN0cmluZz4+KSB7XHJcbiAgICAgICAgbGV0IHRleHQgPSBcIjx0YWJsZT5cIiA7XHJcbiAgICAgICAgZm9yKGxldCBpPTA7IGk8Y2FyZC5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICB0ZXh0ICs9IFwiPHRyPlwiIDtcclxuICAgICAgICAgICAgZm9yKGxldCBrPTA7IGs8Y2FyZFtpXS5sZW5ndGg7IGsrKykge1xyXG4gICAgICAgICAgICAgICAgdGV4dCArPSBgPHRkPiR7Y2FyZFtpXVtrXX08L3RkPmAgOyAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0ZXh0ICs9IFwiPC90cj5cIiA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRleHQgKz0gXCI8L3RhYmxlPlwiIDtcclxuICAgICAgICByZXR1cm4gdGV4dCA7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvdyhjYXJkczogQXJyYXk8QXJyYXk8QXJyYXk8c3RyaW5nPj4+KSB7XHJcbiAgICAgICAgdGhpcy50YWcuaW5uZXJIVE1MID0gXCJcIiA7XHJcbiAgICAgICAgZm9yKGxldCBpZHg9MDsgaWR4PGNhcmRzLmxlbmd0aDsgaWR4Kyspe1xyXG4gICAgICAgICAgICBsZXQgY2FyZCA9IGNhcmRzW2lkeF0gO1xyXG4gICAgICAgICAgICB0aGlzLnRhZy5pbm5lckhUTUwgKz0gdGhpcy50YWJsZShjYXJkKSA7XHJcbiAgICAgICAgICAgIHRoaXMudGFnLmlubmVySFRNTCArPSBcIjxiciAvPlwiIDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19

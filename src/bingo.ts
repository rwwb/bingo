
/**
 * The classes to create a Bingo game 
 */
export enum GameModes {mixed, showA, showB} ;

// the Bingo app
export class BingoGame {
    
    // 
    busy: boolean ;

    current: (x: string) => void ;
    past: (shown: string, other: string)  => void;

    _game_mode: GameModes ;
    _content: Array<[string, string]> ;
    _used: Array<[string, string]> ;

    // The constructor
    constructor(current:  (x: string) => void, past: (shown: string, other: string)  => void) {
        this.busy = false ;
        this.current = current ;
        this.past = past ;

        this._game_mode = GameModes.mixed ;
        this._content = new Array<[string, string]>() ;
        this._used = new Array<[string, string]>() ;
    }

    set gameMode(gm: GameModes) {
        this._game_mode = gm ;
    }

    get gameMode(): GameModes {
        return this._game_mode ;
    }

    // (shallow) copy the content
    set content(cn: Array<[string, string]>) {
        this._content = cn.slice() ;
        this._used = new Array<[string, string]>() ;
    }

    get content(): Array<[string, string]> {
        return this._content ;
    }

    // do the next draw
    next() : number {
        if(this.busy){
            console.log("already running next or reset") ;
            return -1 ;
        }
        this.busy = true ;
        let index = this._next() ;
        this.busy = false ;
        if(index == -1){
            this.current("Game over") ;
        }
        return index ;
    }
        
    // reset the game
    reset() {        
        if(this.busy){
            console.log("already running next or reset") ;
            return ;
        }
        this.busy = true ;
        this._reset() ;
        this.busy = false ;
    }

    // create a new bingo card
    createCard(rows: number, columns: number): Array<Array<string>> {
        let retval: Array<Array<string>> = [] ;

        // really inefficient, but the simplest way to make sure we don't select doubles
        let content = this._content.slice() ;

        // foreach row add a new row to the output
        for(let r=0; r<rows; r++){
            retval.push([]) ;

            // pick a label from the content
            for(let c=0; c<columns; c++){

                // no more elements left to chose from
                if(content.length == 0){
                    retval[r].push("") ;
                    continue ;
                }

                let idx: number = Math.floor(Math.random() * content.length) ;
                let value: [string, string] = content.splice(idx, 1)[0] ;

                // select the label
                let label = "" ;
                switch(this._game_mode){
                    case GameModes.mixed:
                        label = value[Math.floor(Math.random() * 2)] ;
                        break ;
                    case GameModes.showA:
                        label = value[1] ;
                        break ;
                    case GameModes.showB:
                        label = value[0] ;
                        break ;
                    default:
                        break ;
                }
                // add the label to the return value
                retval[r].push(label) ;
            }    
        }
        return(retval) ;
    }

    //
    // Private functions
    //

    private _next() {

        // game over out of elements
        if(this._content.length == 0){
            return -1
        }
    
        // choose the next value to get and remove it from the content
        let index: number = Math.floor(Math.random() * this._content.length) ;
        let value: [string, string] = this._content.splice(index, 1)[0] ;

        // first add it to the already used data
        this._used.push(value) ;

        // Get thedisplay label
        let label: string = "" ;
        let other: string = "" ;
        switch(this._game_mode){
            case GameModes.mixed:
                let i: number = Math.floor(Math.random() * 2) ;
                label = value[i] ;
                other = i == 1 ? value[0] : value[1] ;
                break ;
            case GameModes.showA:
                label = value[0] ;
                other = value[1] ;
                break ;
            case GameModes.showB:
                other = value[0] ;
                label = value[1] ;
                break ;
            default:
                break ;
        } 

        // Show the display label and add it to the list
        this.current(label) ;

        // append the value to the list
        this.past(label, other) ;

        // return the index of the current pick
        return index ;        
    }

    private _reset(){
        for(let i=0; i<this._used.length; i++){
            this._content.push(this._used[i]) ;
        }
        this._used = new Array<[string, string]>() ;
    }
}



/**
 * Renderers to show dynaomic content from the Bingo game
 * 
 */

// A class to render labels on the main screen.
export class LabelRenderer {
    tag: HTMLElement ;

    constructor(tag: HTMLElement) {
        this.tag = tag ;
    }

    show(label: string) {
        this.tag.innerHTML = label ;
    }

    reset() {
        this.tag.innerHTML = "Click next" ;
    }
}

// a class to show the answers
export class AnswerRenderer {
    tag: HTMLElement ;
    answers: Array<Array<string>> ;
    _showAnswers: boolean ;

    constructor(tag: HTMLElement) {
        this.tag = tag ;
        this.answers = Array<Array<string>>() ;
        this._showAnswers = false ;
    }

    set showAnswers(val: boolean) {
        this._showAnswers = val ;
    }

    get showAnswers(): boolean {
        return this._showAnswers ;
    }
    add(label: string, answer: string){
        this.answers.push([label, answer]) ;
    }

    show() {
        if(this.answers.length == 0){
            this.tag.innerHTML = "No previous picks" ;
            return ;
        }

        // with answers
        if(this.showAnswers) {
            let text: string = "<table><tr><th>#</th><th>Shown</th><th>Synonym</th></tr>" ;
            for(let i:number=0;i<this.answers.length; i++){
                let x = this.answers[i] ;
                text += `<tr><td>${i + 1}</td><td>${x[0]}</td><td>${x[1]}</td></tr>` ;
            }
            text += "</table>" ;
            this.tag.innerHTML = text ;
        // without answers
        } else {
            let text: string = "<table><tr><th>#</th><th>Shown</th></tr>" ;
            for(let i:number=0;i<this.answers.length; i++){
                let x = this.answers[i] ;
                text += `<tr><td>${i + 1}</td><td>${x[0]}</td></tr>` ;
            }
            text += "</table>" ;
            this.tag.innerHTML = text ;
        }
        
    }

    reset(){
        this.answers = Array<Array<string>>() ;
        this.tag.innerHTML = "" ;
    }
}

// Render a bingo card
export class CardRenderer {
    tag: HTMLElement ;
    card: Array<Array<string>> ;
    
    constructor(tag: HTMLElement) {
        this.tag = tag ;
    }

    table(card: Array<Array<string>>) {
        let text = "<table>" ;
        for(let i=0; i<card.length; i++) {
            text += "<tr>" ;
            for(let k=0; k<card[i].length; k++) {
                text += `<td>${card[i][k]}</td>` ;    
            }
            text += "</tr>" ;
        }
        text += "</table>" ;
        return text ;
    }

    show(cards: Array<Array<Array<string>>>) {
        this.tag.innerHTML = "" ;
        for(let idx=0; idx<cards.length; idx++){
            let card = cards[idx] ;
            this.tag.innerHTML += this.table(card) ;
            this.tag.innerHTML += "<br />" ;
        }
    }
}


import { GameModes, BingoGame } from "./bingo" ;
import { LabelRenderer, AnswerRenderer, CardRenderer} from "./renderers" ;

// Main game initialization
//
function initialize(){
    // Prepare the renderers
    let cardrenderer = new CardRenderer(document.getElementById("cardholder")) ;
    let picked = new LabelRenderer(document.getElementById("current_pick")) ;
    let answers = new AnswerRenderer(document.getElementById("answer_table")) ;

    // Setup the Bingo game
    let bingo = new BingoGame(
        (label: string) => {
            picked.show(label) ;
        },
        (label: string, other: string) => {
            answers.add(label, other);
        }
    ) ;


    document.getElementById("reset").addEventListener("click", () => {
        bingo.reset() ;
        picked.reset() ;
        answers.reset() ;
    }, false) ;

    document.getElementById("settings").addEventListener("click", () => {
        let el: HTMLElement = document.getElementById("options") ;
        el.style.visibility =  el.style.visibility == "hidden" ? "visible" : "hidden";
    }, false) ;

    document.getElementById("list").addEventListener("click", () => {
        answers.showAnswers = false ;
        answers.show() ;
        let el: HTMLElement = document.getElementById("answers") ;
        el.style.visibility =  el.style.visibility == "hidden" ? "visible" : "hidden" ;
    }, false) ;

    document.getElementById("show_answers").addEventListener("click", () => {
        answers.showAnswers = answers.showAnswers ? false : true ;
        answers.show() ;
    }, false) ;

    document.getElementById("makecard").addEventListener("click", () => {
        let el: HTMLElement = document.getElementById("cards") ;
        el.style.visibility =  el.style.visibility == "hidden" ? "visible" : "hidden";
    }, false) ;

    //
    // Timer functions
    //
    
     // Add the bingo controls
     document.getElementById("next").addEventListener("click", () => {
        bingo.next();
        document.getElementById("options").style.visibility = "hidden" ;
    }, false) ;

    let timer: any = undefined ;
    let updatetime: number = 500 ;
    document.getElementById("run").addEventListener("click", () => {

        // get the interval
        let el = <HTMLInputElement> document.getElementById("interval") ;
        let timespan: number = parseFloat(el.value) * 1000 ;

        // call bingo next when the timer reaches 0
        document.getElementById("countdown").textContent = `${Math.round(timespan / 1000)} s` ;
        let curtime: number = timespan ;
        timer = setInterval( ()=> {
            curtime -= updatetime ;
            if(curtime == 0) {
                bingo.next() ;
                curtime = timespan ;
            }
            document.getElementById("countdown").innerText = `${Math.round(curtime / 1000)} s` ;
        }, updatetime) ;

        // set the options to hidden
        document.getElementById("options").style.visibility = "hidden" ;
        bingo.next() ;
    }, false) ;

    document.getElementById("stop").addEventListener("click", () => {
        console.log("Stop called") ;
        if(timer)
            clearInterval(timer) ;

        timer = undefined ;
    }, false) ;

    //
    // Game altering functions
    //

    // select the game mode
    document.getElementById("gametype_1").addEventListener("change", (evt:any) => {
        if(evt.target.checked)
            bingo.gameMode = GameModes.mixed ;
    }, false) ;

    document.getElementById("gametype_2").addEventListener("change", (evt:any) => {
        if(evt.target.checked)
            bingo.gameMode = GameModes.showA ;
    }, false) ;

    document.getElementById("gametype_3").addEventListener("change", (evt:any) => {
        if(evt.target.checked)
            bingo.gameMode = GameModes.showB ;
    }, false) ;

    // add the content loader
    document.getElementById("files").addEventListener("change", (evt:any) => {

        // only load the first file
        let curfile = evt.target.files[0] ;
        let reader = new FileReader() ;
        reader.onload = () => {
            // get the raw text 
            let raw: string = reader.result ;
            
            // split the string on newlines and tabs
            const res = raw.split("\n").map<[string, string]>((line) => {
                let el: Array<string> = line.split("\t") ;
                if(el.length != 2)
                    return ;
                return [el[0], el[1]] ;
            }) ;

            // add the content to bingo
            bingo.content = res.filter(el => el != undefined) ;
            answers.reset() ;
            picked.reset() ;
        }
        reader.readAsText(curfile) ;
    }, false) ;

    //
    // Card generation
    //

    document.getElementById("generate").addEventListener("click", () => {

        // 
        let width = parseInt((<HTMLInputElement> document.getElementById("card_width")).value) ;
        let height = parseInt((<HTMLInputElement> document.getElementById("card_height")).value) ;
        let number = parseInt((<HTMLInputElement> document.getElementById("card_number")).value) ;
        
        // generate number cards
        let cards: Array<Array<Array<string>>> = new Array<Array<Array<string>>>() ;
        for(let idx=0; idx<number; idx++){
            let card: Array<Array<string>> = bingo.createCard(height, width) ;
            cards.push(card) ;
        }

        // show the cards onscreen
        cardrenderer.show(cards) ;
    }, false) ;


    document.getElementById("card_download").addEventListener("click", () => {
        // adapted from https://jsfiddle.net/78xa14vz/3/
        let css: string = 
            "<style>" + 
            '@page WordSection1{size: 595.35pt 841.95pt;mso-page-orientation: portrait;}' +
            'div.WordSection1 {page: WordSection1;}' +
            'table{border-collapse:collapse;}td{border:1px gray solid;width:5em;padding:2px;}'+
            "</style>" ;
        let html = "<div class='WordSection1'>" + cardrenderer.tag.innerHTML + "</div>" ;
        let blob: Blob = new Blob(['\ufeff', css + html], {
            type: 'application/msword'
        });

        // download in IE
        if(navigator.msSaveOrOpenBlob){
            navigator.msSaveOrOpenBlob(blob, "bingo_cards.doc") ;
            // download in other browsers
        } else {
            let url: string = URL.createObjectURL(blob) ;
            let link: HTMLLinkElement = <HTMLLinkElement>document.createElement('A') ;
            link.href = url ;
            document.body.appendChild(link) ;
            link.click() ;
            document.body.removeChild(link) ;
        }
    }, false) ;
}

// initialize the game
initialize() ;


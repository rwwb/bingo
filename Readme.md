Bingo
=====

Description
-----------

This repository contains a digital Bingo game designed for education purposes. The players get a word on the screen and should cross a corresponding word on their cards. The words to be displayed and crossed are retrieved from a tab-delimited list that can be easily created in Microsoft Excel by selecting save as tab-delimited text in the save as options. An example tab-delimited file is available [here](test_data/zouten.txt).

The words can be cycled through with a button or with an automated timer. The word history is diplayed on the right hand side. We are currently updating the UI, so the look of the tool will change.

Use
---

After downloading this repository, one can simply open the `dist/index.html` file in a browser. We have tested the game in Google Chrome, Mozilla FireFox, Microsoft Internet Explorer, and Microsoft Edge. However our tests are currently not in depth and Bugs may still be present. Please submit them to the bug tracker.

The game can also be downloaded from the **Downloads** section. However these might not always be up to date.

Tutorial
--------

For an english tutoral on the bingo game, click [here](images/tutorial_en.md)

To do for version 3
-------------------

* Add different language optionslude localisation options.

Development and Building
------------------------

For the game to be functional the TypeScript code needs to be transpiled to javascript. To transpile the code, gulp is used as indicated in the tutorial [here](http://www.typescriptlang.org/docs/handbook/gulp.html). The current gulp tasks include all steps upto `Watchify`. Uglify and Babel are not (yet) used as mangled code or supporting old browsers is not a priority. `Watchify` will rebuild the javascript portions of the program everytime a change is made in the TypeScript. Changes in the `css` and ``html` portions require gulp to be restarted before they are incorporated. I should also set copy-on-change up for these assets.

```build_code
# first time setup of the build environment
npm install -g gulp-cli
npm install

# continuous build
gulp
```

To end the continuous build use the key combination `ctrl` + `C`. The last build bundle should be present in the `dist` folder.
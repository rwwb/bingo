Tutorial
========

This tutorial will show you how to download and run the bingo V2 game.

Download, unpack and start the game
-----------------------------------

* The bingo game can be downloaded the game from the git repository. The easiest way is to download the game from the `Downloads` section at the left hand side menu.

* Afterwards you downloaded the game, go to the location where saved the zip file in the file explorer.

* Unpack the zip file with the `unpack` option located in the right mouse button context menu.

* Go to the newly created dist folder and click on the  `index.html` file.

* A new browser window should popup with the Bingo game. If your default browser is internet explorer, click the allow blocked content button.

Playing the game
----------------

### General explanations

The bingo will draw entries from a 2 column list. The aim for students is to determine whether this entry or its synonym is on their card. After a set amount of time the next entry is drawn.

* After starting the game, you will be presented with a screen as in figure 1. This is the main screen of the game.

![Figure 1](01_options.png)
Figure 1.The options screen.

* On the top row, there are 2 groups of icons. The left group of icons are the game controls. The right group of icons are the game functions. In the center a count down timer is located.

  * The game control icons are the `start`, `stop` and `next` controls. These start and stop the countdown to the next draw. Next draws the next entry immediately.

  * In the middle, the time is displayed until the next draw.

  * The game function icons represent from left to right `card creation`, `show answers`, `reset game` and `show options`.

    * **Clicking the icon for a function will make the relevant page appear. To close the page, click the icon again.**

    * Note that the reset icon has no page and simply resets the game.

### Starting a game

* To start a game, we first need a list to draw from. The data should be provided as a 2 column tab-delimited file. These can be created in MS Excel by choosing `text tab-delimited` in the `save as` menu. An example of a tab-delimited input file ([zouten.txt](../test_data/zouten.txt)) is procided in the `test_data` directory. The words in the tab-delimited file can be any HTML code.

* Upload the file to the game with the `browse files` button in the options screen.

  * There are 3 game modes: mixed, show A and show B.

  * In mixed the entries in columns 1 and 2 are shown randomly and either can be on the cards

  * In show A, only the entries in column A are shown and column B should be guessed.

  * In show B, only the entries in column B are shown and column A should be guessed.

  * The delay before switching to the next entry is given in the `Timer` field.

* After clicking the play button, the option page is hidden and the view switches to figure 2. On the center of this page, the word is shown.

![Figure 2](02_go.png)
Figure 2.The playing field.

### Checking scores

* When a player has bingo,the game can be stopped and the answers checked (Figure 3). In the answer field, the answers are displayed in a table.

  * The corresponding synonyms can be toggled on and off using the question mark icon.

![Figure 3](03_answers.png)
Figure 3.The answer table.

### Creating cards

* The bingo game can also create the bingo cards. To create cards, select the `card creation` option after setting the game mode and uploading the entries. You should be presented with a view such as figure 4A.

  * In the card creation, you can specify the number of rows and columns of the cards, as well as the number of cards to generate.

  * By pressing the generate button, the specified number of cards will be generated with random entries from the uploaded list. These cards will be displayed as in figure 4B.

  * To download the cards and open them in Microsoft word, one can use the `W` icon beneath the generate button.

![Figure 4A](04a_cards.png)
Figure 4A. Card creation options.
 
![Figure 4B](04b_cards.png)
Figure 4B. Card creation result.
